<br>
<h2>WebControl.link - control your home over the web</h2>
<br><br>
Instructions
<br><br>                        
<p>Download the latest image of Raspbian Jessie - <a href="https://www.raspberrypi.org/downloads/raspbian/" target="_blank">Link for download</a></p>                        
<p><b>Install the prerequisite software and libraries</b></p>
<p>Install wiringPi - <a href="http://wiringpi.com/download-and-install/" target="_blank">Link for download</a></p>
<p><pre>
apt-get update
apt-get dist-upgrade
sudo apt-get install python-pip
sudo apt-get install python-urllib3
sudo apt-get install python3-pifacedigital-emulator
sudo apt-get install php5-curl
sudo apt-get install raspi-config
sudo apt-get install python-dev
sudo apt-get install libboost-python-dev
sudo apt-get install rpm
sudo apt-get install git-core
sudo pip install pi_switch
sudo pip install pika
sudo pip install requests
sudo pip install pycrypto   # for windows - pip install pycryptodoom
</pre></p>
<p><b>Now start raspi-config by typing</b><br>
<code>sudo raspi-config</code></p>
<p>To view the list of advanced configuration options, select <code>Option 8 Advanced Options</code>.<br>
Choose the <code>A5 SPI</code> option. Set this to <code>"Yes"</code><br>
<p>Install the webcontrol package</p>                       
<p><pre>
sudo su -
wget "https://gitlab.com/vasilev.link/webcontrol/-/archive/master/webcontrol-master.zip" -O /var/tmp/master.zip
unzip -o -u /var/tmp/master.zip -d /var/tmp
cp -R /var/tmp/webcontrol-master/etc/* /etc/
cp -R /var/tmp/webcontrol-master/var/* /var/
rm -Rf /var/tmp/master.zip /var/tmp/webcontrol-master*
</pre><br><br>
The package is installing crontab, which starts every 10 minutes and check if there is a webcontrol process, if not - start it automatically.
