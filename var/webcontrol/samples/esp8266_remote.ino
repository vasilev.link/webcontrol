/*
   Universal ESP8266 control script
   created by Vasilev.link
   for WebControl.link purpose
   make GET request to the Wemos/NodeMCU/ESP8266 module:
   - for led control
   http://IP_Address/username/uniqid/rgb/GPIO_PIN,R,G,B
   - for relay
   http://IP_Address/username/uniqid/relay/GPIO_PIN,1|0
   - for temperature
   http://IP_Address/username/uniqid/temp/GPIO_PIN
*/
#include <ESP8266WiFi.h>
#include "DHT.h"
#include <Adafruit_NeoPixel.h>
WiFiServer server(80);

const char* ssid     = "WIFI USER";
const char* password = "WIFI PASS";
String qs = "/username from WebControl.link/Unique ID from WebControl.link";
String suid = "server id from WebControl.link";

void setup() {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  server.begin();
  Serial.begin(115200);
}

void loop() {
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  String request = client.readStringUntil('\r');
  client.flush();
  if (request.indexOf(qs) != -1) {
    String getrequest = request.substring(request.indexOf(' ') + 1, request.indexOf(' ', request.indexOf(' ') + 1));
    int fparamind = getrequest.indexOf('/');
    int sparamind = getrequest.indexOf('/', fparamind + 1);
    int tparamind = getrequest.indexOf('/', sparamind + 1);
    int foparamind = getrequest.indexOf('/', tparamind + 1);
    int fiparamind = getrequest.indexOf('/', foparamind + 1);
    String tparam = getrequest.substring(tparamind + 1, foparamind);
    String foparam = getrequest.substring(foparamind + 1, fiparamind);
    if (tparam == "rgb") {
      int fcomind = foparam.indexOf(',');
      int scomind = foparam.indexOf(',', fcomind + 1);
      int tcomind = foparam.indexOf(',', scomind + 1);
      int focomind = foparam.indexOf(',', tcomind + 1);
      String pin = foparam.substring(0, fcomind);
      String r = foparam.substring(fcomind + 1, scomind);
      String g = foparam.substring(scomind + 1, tcomind);
      String b = foparam.substring(tcomind + 1, focomind);
#define PIN            pin.toInt()
      Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, PIN, NEO_GRB + NEO_KHZ800);
      pixels.begin();
      pixels.setPixelColor(0, pixels.Color(r.toInt(), g.toInt(), b.toInt()));
      pixels.show();
      client.println("{\"data\":\"led is with color r:" + r + " g:" + g + " b:" + b + "\"}");
      delay(1);
    } else if (tparam == "relay") {
      int fcomind = foparam.indexOf(',');
      int scomind = foparam.indexOf(',', fcomind + 1);
      String pin = foparam.substring(0, fcomind);
      String rel = foparam.substring(fcomind + 1, scomind);
      const int relayPin = pin.toInt();
      pinMode(relayPin, OUTPUT);
      if (rel == "1") {
        digitalWrite(relayPin, HIGH);
        client.println("{\"data\":\"Relay started\"}");
      } else {
        digitalWrite(relayPin, LOW);
        client.println("{\"data\":\"Relay stopped\"}");
      }
      delay(1);
    } else if (tparam == "temp") {
      DHT dht;
      dht.setup(foparam.toInt());
      delay(1000);
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/plain;charset=utf-8");
      client.println();
      float humidity = dht.getHumidity();
      float temperature = dht.getTemperature();
      client.print("{\"server\":\"" + suid + "\",");
      client.print("\"humidity\":\"" + String(humidity) + "\",");
      client.print("\"temp\":\"" + String(temperature) + "\"");
      client.print("}");
    } else {
      client.println("{\"error\":\"Requested method not added\"}");
    }
  } else {
    client.println("{\"log\":\"Please provide the right UID and username\"}");
  }
  client.println();
  delay(1);
  return;
}
