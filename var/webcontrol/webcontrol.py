#Created by Vasilev.link. For WebControl purpose only
#!/usr/bin/python
from modules import main
from modules import messagetypes

urldata = main.urllib.urlencode({'ssl_options': {'ca_certs': main.dir_path+"/certs/comodo.pem"}})
amqpurl = main.os.environ.get('CLOUDAMQP_URL', 'amqps://'+main.mqttclientid+':'+main.mqttpass+'@'+main.mqtthost+'/'+main.mqttclientid+'?'+urldata)
params = main.pika.URLParameters(amqpurl)
params.socket_timeout = 5
connection = main.pika.BlockingConnection(params)
channel = connection.channel()
result = channel.queue_declare(queue=main.mqtttopic,auto_delete=True,durable=True,exclusive=True)
queue_name = result.method.queue
channel.queue_bind(exchange='amq.topic', queue=queue_name)

def callback(ch, method, properties, body):
  tree = main.ET.fromstring(body)
  control = tree.find('.//data')
  if str(control.attrib['uid'])==main.webuid:
     try:
        mess_data=main.decryptmess(str(control.attrib['data']))
     except:
        mess_data=main.json.loads('{"type":"not_decoded"}')
     if str(mess_data['type'])=="piface":
        messagetypes.pifacemes(int(mess_data['num']),int(mess_data['val']),int(mess_data['board']))
     elif str(mess_data['type'])=="433":
        messagetypes.mes433(int(mess_data['num']),str(mess_data['method']),int(mess_data['board']),int(mess_data['okey']))
     elif str(mess_data['type'])=="command":
        messagetypes.comms(str(mess_data['val']),str(mess_data['syncinfo']))
     elif str(mess_data['type'])=="timeauto":
        messagetypes.homeauto_time(str(mess_data['method']),str(mess_data['sttype']),int(mess_data['stnum']),int(mess_data['stval']),int(mess_data['stboard']),str(mess_data['sttime']),str(mess_data['stdate']),str(mess_data['enddate']),str(mess_data['homeid']),int(mess_data['enabled']))
     elif str(mess_data['type'])=="sensor":
        messagetypes.sensor(str(mess_data['method']),str(mess_data['sensid']),str(mess_data['senstype']),str(mess_data['sensinfo']),str(mess_data['sensip']),str(mess_data['senspin']))
     else:
        print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown type received:"+str(mess_data['type'])
  else:
     print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown UID received:"+str(control.attrib['uid'])

channel.basic_consume(callback,queue=queue_name,no_ack=True)
channel.start_consuming()
connection.close()
