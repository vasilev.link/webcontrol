#!/bin/bash
#Created by Vasilev.link. For WebControl purpose only
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
if [ -f "/var/webcontrol/stopmon.txt" ]; then
	echo "$TIMESTAMP monitoring stopped"
elif [ -f "/var/webcontrol/ha/syncoffline_temp.txt" ]; then
        php -q /var/webcontrol/dojobs.php syncoffline
	echo "$TIMESTAMP sync job triggered"
elif [ -f "/var/webcontrol/doupgrade.txt" ]; then
        source "/var/webcontrol/webconfig.conf"
        ps -ef|grep python | grep webcontrol | awk {'print $2'} | xargs kill -9
	/usr/bin/wget "https://github.com/vaskovasilev/webcontrol/archive/master.zip" -O /var/tmp/master.zip
	unzip -o -u /var/tmp/master.zip -d /var/tmp; cp -R /var/tmp/webcontrol-master/etc/* /etc/; cp -R /var/tmp/webcontrol-master/var/* /var/
	rm -Rf /var/tmp/master.zip /var/tmp/webcontrol-master*
	rm -Rf /var/webcontrol/doupgrade.txt
	php -q /var/webcontrol/dojobs.php
	/var/webcontrol/control_mqtt.sh
	curl -d user=$webcontroluser -d servid=$thisserver -d uid=$webuid https://webcontrol.link/api/status-controller-updated
	echo "$TIMESTAMP upgrade finished"
else
	df -H | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5":"$6":"$3":"$2 }' > /var/webcontrol/tmp/diskusage.txt
	ps -eo pcpu,pmem,pid,user,args --no-headers| sort -t. -nk1,2 -k4,4 -r |head -n 5 | awk {'print $1":"$2":"$3":"$4":"$5$NF'} > /var/webcontrol/tmp/memusage.txt
	uptime -p > /var/webcontrol/tmp/uptime.txt
		if [ -d "/opt/vc" ]; then
		/opt/vc/bin/vcgencmd measure_temp > /var/webcontrol/tmp/temp.txt
		else 
		echo "0" > /var/webcontrol/tmp/temp.txt
		fi
	php -q /var/webcontrol/dojobs.php
	/var/webcontrol/control_mqtt.sh
	echo "$TIMESTAMP base tasks finished"
fi
