#Created by Vasilev.link. For WebControl purpose only
#!/usr/bin/python
from . import main
def pifacesm(board):
    f = open(main.dir_path+"/iotadmin_reset.json", 'r+')
    resetjson = main.json.load(f)
    if board:
       for key in resetjson.keys(): main.p.digital_write(int(key),int(resetjson[key]),int(board))
    else:
       for key in resetjson.keys(): main.p.digital_write(int(key),int(resetjson[key]))
    f.close()
def pifacemes(pin,val,board):
    if board:
       main.p.digital_write(pin,val,board)
    else:
       main.p.digital_write(pin,val)
    r=main.requests.post("https://webcontrol.link/api/status-output", headers=main.headers, data={'user': main.webcontroluser, 'server': main.thisserver, 'output': pin, 'status': val, 'type':"piface"}, timeout=1)
    f = open(main.dir_path+"/iotadmin_reset.json", 'r+')
    resetjson = main.json.load(f)
    resetjson[str(pin)] = str(val)
    f.seek(0)
    main.json.dump(resetjson,f,separators=(',', ':'))
    f.close()
    print main.time.strftime("%Y-%m-%d %H:%M:%S")+": send value:"+str(val)+" to output pin:"+str(pin)
def mes433(code,method,bitl,numid):
    if method=="scan":
        receiver = main.pi_switch.RCSwitchReceiver()
        receiver.enableReceive(1) # use WiringPi pin 1
        code=""
        t_end = main.time.time() + 10
        while main.time.time() < t_end:
            if receiver.available():
               received_value = receiver.getReceivedValue()
               if received_value:
                   code="%s,%s" % (received_value, receiver.getReceivedBitlength())
               receiver.resetAvailable()
        r=main.requests.post("https://webcontrol.link/api/status-output", headers=main.headers, data={'user': main.webcontroluser, 'server': main.thisserver, 'output': code, 'id': numid, 'type':"433"}, timeout=1)
        print main.time.strftime("%Y-%m-%d %H:%M:%S")+": send code:"+str(code)+" for id:"+str(numid)
    else:
        sender = main.pi_switch.RCSwitchSender()
        sender.enableTransmit(0) # use WiringPi pin 0
        sender.setPulseLength(301)
        sender.sendDecimal(int(code),int(bitl))
        print main.time.strftime("%Y-%m-%d %H:%M:%S")+": send 433MHz signal:"+str(code)
def comms(command,info):
    if command=="upgrade":
       if main.os.path.isfile(main.dir_path+"/doupgrade.txt"):
          main.os.remove(main.dir_path+"/doupgrade.txt")
       main.os.mknod(main.dir_path+"/doupgrade.txt", 0644)
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": execuded command:"+str(command)
    elif command=="stopmon":
       if main.os.path.isfile(main.dir_path+"/stopmon.txt"):
          main.os.remove(main.dir_path+"/stopmon.txt")
       main.os.mknod(main.dir_path+"/stopmon.txt", 0644)
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": execuded command:"+str(command)
    elif command=="syncoffline":
       if main.os.path.isfile(main.dir_path+"/ha/syncoffline_temp.txt"):
          main.os.remove(main.dir_path+"/ha/syncoffline_temp.txt")
       f = open(main.dir_path+'/ha/syncoffline_temp.txt', 'w')
       f.write(str(info))
       f.close()
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": sync data uploaded. execuded command:"+str(command)
    elif command=="enmon":
       if main.os.path.isfile(main.dir_path+"/stopmon.txt"):
          main.os.remove(main.dir_path+"/stopmon.txt")
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": execuded command:"+str(command)
    else:
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown command received:"+str(command)
def homeauto_time(method,sttype,stpin,stval,stboard,sttime,stdate,enddate,homeid,enabled):
    if method=="start":
       if main.os.path.isfile(main.dir_path+"/ha/time/"+homeid+".conf"):
          main.os.remove(main.dir_path+"/ha/time/"+homeid+".conf")
       main.os.mknod(main.dir_path+"/ha/time/"+homeid+".conf", 0644)
       fa = open(main.dir_path+"/ha/time/"+homeid+".conf", "w")
       fa.write("c_starttype=\""+str(sttype)+"\"\r\n")
       fa.write("c_startpin=\""+str(stpin)+"\"\r\n")
       fa.write("c_startval=\""+str(stval)+"\"\r\n")
       fa.write("c_startboard=\""+str(stboard)+"\"\r\n")
       fa.write("c_sttime=\""+str(sttime)+"\"\r\n")
       fa.write("c_stdate=\""+str(stdate)+"\"\r\n")
       fa.write("c_enddate=\""+str(enddate)+"\"\r\n")
       fa.write("c_enabled=\""+str(enabled)+"\"\r\n")
       fa.close()
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": created new time automation. Monitor ID: "+homeid
    elif method=="enable":
       if main.os.path.isfile(main.dir_path+"/ha/time/"+homeid+".conf_disabled"):
          main.os.rename(main.dir_path+"/ha/time/"+homeid+".conf_disabled",main.dir_path+"/ha/time/"+homeid+".conf")
          print main.time.strftime("%Y-%m-%d %H:%M:%S")+": monitor enabled. Monitor ID: "+homeid
    elif method=="disable":
       if main.os.path.isfile(main.dir_path+"/ha/time/"+homeid+".conf"):
          main.os.rename(main.dir_path+"/ha/time/"+homeid+".conf",main.dir_path+"/ha/time/"+homeid+".conf_disabled")
          print main.time.strftime("%Y-%m-%d %H:%M:%S")+": monitor disabled. Monitor ID: "+homeid
    elif method=="end":
       if main.os.path.isfile(main.dir_path+"/ha/time/"+homeid+".conf"):
          main.os.remove(main.dir_path+"/ha/time/"+homeid+".conf")
          print main.time.strftime("%Y-%m-%d %H:%M:%S")+": deleted one time automation. Monitor ID: "+homeid
    else:
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown method received:"+str(method)
def sensor(method,sensid,senstype,sensinfo,sensip,senspin):
    if method=="create":
       if main.os.path.isfile(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf"):
          main.os.remove(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf")
       main.os.mknod(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf", 0644)
       fa = open(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf", "w")
       fa.write("s_ip=\""+str(sensip)+"\"\r\n")
       fa.write("s_pin=\""+str(senspin)+"\"\r\n")
       fa.write("s_info=\""+str(sensinfo)+"\"\r\n")
       fa.write("s_type=\""+str(senstype)+"\"\r\n")
       fa.close()
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": created new sensor. Sensor id: "+str(sensid)
    elif method=="light":
       r=main.requests.get("http://"+sensip+"/"+main.webcontroluser+"/"+sensid+"/rgb/"+senspin+","+sensinfo, headers=main.headers, timeout=2)
       resp=r.json()
       main.time.sleep(1)
       respinfo=resp["data"] if "data" in resp else ""
       respdata={'user':main.webcontroluser,'uid':sensid,'serverip':sensip,'info':respinfo}
       r=main.requests.post("https://webcontrol.link/api/status-device", headers=main.jsonheaders, data=main.json.dumps(respdata), timeout=1)
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": send RGB color("+sensinfo+") to controller: "+str(sensid)
    elif method=="delete":
       if main.os.path.isfile(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf"):
          main.os.remove(main.dir_path+"/monitoring/"+senstype+"/"+sensid+".conf")
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": deleted one sensor. Sensor ID: "+sensid
    else:
       print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown method received:"+str(method)
