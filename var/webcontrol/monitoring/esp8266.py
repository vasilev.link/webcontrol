import sys
sys.path.append('/var/webcontrol/')
from modules import main
from modules import messagetypes

for file in main.os.listdir("/var/webcontrol/monitoring/esp8266"):
    if file.endswith(".conf"):
       main.getVarFromFile("/var/webcontrol/monitoring/esp8266/"+file)
       try:
         r=main.requests.get("http://"+main.conf.s_ip+"/"+main.webcontroluser+"/"+file.replace(".conf","")+"/temp/"+main.conf.s_pin)
         dataesp = r.json() 
         r=main.requests.post("https://webcontrol.link/api/status-mon", headers=main.headers, data={'temp':dataesp['temp'],'humidity':dataesp['humidity'],'user': main.webcontroluser, 'type':"esp8266", 'server': main.thisserver, 'uid': file.replace(".conf","")})
       except Exception:
         sys.exc_clear()
            
