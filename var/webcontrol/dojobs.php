<?php
//Created by Vasilev.link. For WebControl purpose only
$file_handle = fopen("/var/webcontrol/webconfig.conf", "r");
while (!feof($file_handle) ) {
  $line_of_text = fgets($file_handle);
  $parts = explode('=', trim($line_of_text) );
  if (strcmp($parts[0], "webcontroluser")==0)
      $webuser = str_replace("\"","",trim($parts[1]));
  if (strcmp($parts[0], "webuid")==0)
     $webuid = str_replace("\"","",trim($parts[1]));
  if (strcmp($parts[0], "suid")==0)
     $suid = str_replace("\"","",trim($parts[1]));
}
if(!empty($webuser)){
  if(!empty($argv[1])){
  if($argv[1]=="syncoffline"){
     if( $file = @file_get_contents('/var/webcontrol/ha/syncoffline_temp.txt',true) ){
         if(!is_dir("/var/www/html/data/")){
          mkdir("/var/www/html/data/", 0755);
         }       
        $alldata=json_decode(gzuncompress(base64_decode(str_replace(" ","+",$file))),true); 
        foreach($alldata as $key=>$val){
        file_put_contents('/var/www/html/data/'.$key.'.json',json_encode($val,true));
        }
        unlink('/var/webcontrol/ha/syncoffline_temp.txt');
     }
  }
 }
 function get_server_memory_usage(){
 $free = shell_exec('free');
 $free = (string)trim($free);
 $free_arr = explode("\n", $free);
 $mem = explode(" ", $free_arr[1]);
 $mem = array_filter($mem);
 $mem = array_merge($mem);
 $memory_usage = $mem[2]/$mem[1]*100;
 return $memory_usage;
 }
 function get_server_cpu_usage(){
 $load = sys_getloadavg();
 return $load[0];
 }
 $fileupt = fopen("/var/webcontrol/tmp/uptime.txt", "r");
 while (!feof($fileupt)) {
 $thisupt[]=str_replace(array(",","
"),"",fgets($fileupt));
 }
 fclose($fileupt);
 $filetemp = fopen("/var/webcontrol/tmp/temp.txt", "r");
 while (!feof($filetemp)) {
 $thistemp[]=str_replace(array("temp=","'C"),"",fgets($filetemp));
 }
 fclose($filetemp);
 $data=array('serv_uptime'=>json_encode($thisupt),'serv_temp'=>"$thistemp[0]",'serv_cpu'=>get_server_cpu_usage(),'serv_mem'=>get_server_memory_usage(),'uniqid' => "$webuid",'sessuserid' => "$webuser",'id' => "$suid");
 $ch = curl_init("https://webcontrol.link/api/status-controller");
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
 $data_string = json_encode($data);
 curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
 $content = curl_exec($ch);
 curl_close($ch);
}
unset($thisfile,$ch,$content,$data_string,$filetemp,$fileupt,$free,$mem,$data);
