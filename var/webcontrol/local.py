#Created by Vasilev.link. For WebControl local use purpose only
#!/usr/bin/python
from modules import main
from modules import messagetypes
mess_data=main.json.loads(main.sys.argv[1])

if str(mess_data['type'])=="piface":
   messagetypes.pifacemes(int(mess_data['num']),int(mess_data['val']),int(mess_data['board']))
elif str(mess_data['type'])=="433":
   messagetypes.mes433(int(mess_data['num']),str(mess_data['method']),int(mess_data['board']),int(mess_data['okey']))
elif str(mess_data['type'])=="command":
   messagetypes.comms(str(mess_data['val']),str(mess_data['syncinfo']))
elif str(mess_data['type'])=="timeauto":
   messagetypes.homeauto_time(str(mess_data['method']),str(mess_data['sttype']),int(mess_data['stnum']),int(mess_data['stval']),int(mess_data['stboard']),str(mess_data['sttime']),str(mess_data['stdate']),str(mess_data['enddate']),str(mess_data['homeid']),int(mess_data['enabled']))
elif str(mess_data['type'])=="sensor":
   messagetypes.sensor(str(mess_data['method']),str(mess_data['sensid']),str(mess_data['senstype']),str(mess_data['sensinfo']),str(mess_data['sensip']),str(mess_data['senspin']))
else:
   print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown command received:"+str(mess_data['type'])
