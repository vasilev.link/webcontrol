import sys
sys.path.append('/var/webcontrol/')
from modules import main
from modules import messagetypes

for file in main.os.listdir("/var/webcontrol/ha/time"):
    if file.endswith(".conf"):
       main.getVarFromFile("/var/webcontrol/ha/time/"+file)
       if main.conf.c_enddate=="0":
           if main.time.strptime(main.conf.c_stdate, "%d%m%Y")<=main.time.strptime(main.time.strftime("%d%m%Y"), "%d%m%Y"):
              if main.time.strftime("%H%M")==main.conf.c_sttime:
                 if str(main.conf.c_starttype)=="piface":
                   messagetypes.pifacemes(int(main.conf.c_startpin),int(main.conf.c_startval),int(main.conf.c_startboard))
                 elif str(main.conf.c_starttype)=="433":
                   messagetypes.mes433(int(main.conf.c_startval),str("control"),int(main.conf.c_startboard),int(main.conf.c_startpin))
                 else:
                   print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown type received:"+str(main.conf.c_starttype)
                 r=main.requests.post("https://webcontrol.link/api/status-ha-time", headers=main.headers, data={'user': main.webcontroluser, 'server': main.thisserver, 'uid': file.replace(".conf","")})
       else:
           if main.time.strptime(main.conf.c_stdate, "%d%m%Y")<=main.time.strptime(main.time.strftime("%d%m%Y"), "%d%m%Y") and main.time.strptime(main.time.strftime("%d%m%Y"), "%d%m%Y")<=main.time.strptime(main.conf.c_enddate, "%d%m%Y"):
              if main.time.strftime("%H%M")==main.conf.c_sttime:
                 if str(main.conf.c_starttype)=="piface":
                   messagetypes.pifacemes(int(main.conf.c_startpin),int(main.conf.c_startval),int(main.conf.c_startboard))
                 elif str(main.conf.c_starttype)=="433":
                   messagetypes.mes433(int(main.conf.c_startval),str("control"),int(main.conf.c_startboard),int(main.conf.c_startpin))
                 else:
                   print main.time.strftime("%Y-%m-%d %H:%M:%S")+": unknown type received:"+str(main.conf.c_starttype)
                 r=main.requests.post("https://webcontrol.link/api/status-ha-time", headers=main.headers, data={'user': main.webcontroluser, 'server': main.thisserver, 'uid': file.replace(".conf","")})
