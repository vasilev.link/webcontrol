#!/bin/bash
#Created by Vasilev.link. For WebControl purpose only
pid=`ps -ef|grep python | grep webcontrol.py | awk '{print $2}'`
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
if [[ -n $pid ]]; then
     :
else
     echo "starting new control"
     /usr/bin/nohup stdbuf -oL /usr/bin/python /var/webcontrol/webcontrol.py > /var/webcontrol/logs/webcontrol.log  2>&1 &
     if [[ $? -eq 0 ]]; then
             echo "$TIMESTAMP control started"
     else
             echo "$TIMESTAMP an error occured. RC was $?"
     fi
fi
