<?php  
  include "conf/config.php";
  $page="controllers";
  sessionClass::page_protect();
  include "modules/headcontent.php";?>
</head>
<body class="fixed-left">
  <div id="wrapper">
    <?php include "modules/menu.php";?>
    <div class="content-page">
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="page-title-box">
                <ol class="breadcrumb pull-right">
                  <li class="active">Controllers</li>
                </ol>
                <h4 class="page-title">&nbsp;</h4>
              </div>
            </div>
          </div>
          <?php 
  $json = file_get_contents('data/controllers.json');
  $zobj = json_decode($json,true);

  if(!empty($zobj)){

        $outputs=$zobj['outputs']; 
        ?> 
          <div class="row">
            <div class="col-md-6">
              <div class="card-box">
                <?php foreach ($outputs as $keydatasn => $valdatasn)
         { if($keydatasn=="piface"){ foreach ($outputs['piface'] as $keydatasn => $valdatasn)
                                              {?>
                <div class="row" style="padding-bottom:5px;">
                  <div class="col-md-10">
                    <?php if($valdatasn['val']=="1"){ ?>  
                    <button type="button" class="btn waves-effect btn-success btn-block butpf<?php echo $valdatasn['output']?> tl" name="butoff" onclick="local('off','<?php echo $valdatasn['output']?>','piface','pf<?php echo $valdatasn['output']?>','<?php echo $zobj['uid'];?>','2');"><i class="fa fa-power-off"></i>&nbsp;Turn <?php echo $valdatasn['info'];?> Off</button>
                    <?php } else { ?>
                    <button type="button" class="btn waves-effect btn-danger btn-block butpf<?php echo $valdatasn['output']?> tl" name="buton" onclick="local('on','<?php echo $valdatasn['output']?>','piface','pf<?php echo $valdatasn['output']?>','<?php echo $zobj['uid'];?>','2');"><i class="fa <?php if(!empty($valdatasn['icon'])){echo $valdatasn['icon'];} else { echo "fa-play";} ?>"></i>&nbsp;Turn <?php echo $valdatasn['info'];?> On</button>
                    <?php } ?>
                  </div>
                  <div class="col-md-2 stout" style="text-align:center;font-size:1.7em;vertical-align:middle;">
                    <i class="fa fa-<?php echo (($valdatasn['val']=="1") ? "check g" : "close r");?>" id="waiticonpf<?php echo $valdatasn['output']?>"></i>
                  </div>   
                </div>
                <?php
}} elseif($keydatasn=="433"){ ?><div class="row" style="padding-bottom:5px;">
                <?php foreach ($outputs['433'] as $keydatasn => $valdatasn)
                                              {?>
                  <div class="col-md-6">
                    <button type="button" class="btn waves-effect btn-<?php echo (!empty($valdatasn['buttype'])? $valdatasn['buttype']: "info");?> btn-block but433<?php echo $keydatasn;?> tl tl433" name="but433" onclick="local('on','<?php echo $valdatasn['code']?>','433','433<?php echo $keydatasn;?>','<?php echo $zobj['uid'];?>','<?php echo $valdatasn['val'];?>');"><i class="fa <?php echo (!empty($valdatasn['icon'])? $valdatasn['icon']: "fa-wifi");?>"></i>&nbsp;<?php echo $valdatasn['info'];?></button>
                  </div>
<?php } ?></div><?php
}
} ?>
              </div>
            </div>  
            <div class="col-md-6">     
              
            </div>
          </div>   
          
         <?php } else { ?>
 <div class="alert alert-warning">Empty file controllers.json . Please click on the button "Sync Offline" from the website first.</div>
          <?php } ?>
          
        </div>
      </div>
<?php include "modules/footer.php";?>
    </div>
  </div>
<?php include "modules/js.php";?>
</body>
</html><?php include "modules/template_end.php";?>
