<?php
class url {
  private $site_path;
  public function __construct($site_path) {
    $this->site_path = $this->removeSlash($site_path);
  }
  private function removeSlash($string) {
    if($string[strlen($string) - 1] == '/')
      $string = rtrim($string, '/');
    return $string;
  }
  public function part($part) {
    $url = str_replace($this->site_path, '',$_SERVER['REQUEST_URI']);
    $url = explode('/', $url);
    if(isset($url[$part]))
      return $url[$part];    
    else
      return false;
  }
}
class urlchangeClass{
  public static function EncodeURL($url)
  {
    $new = strtolower(ereg_replace(' ','_',$url));
    return($new);
  }
  public static function DecodeURL($url)
  {
    $new = ucwords(ereg_replace('_',' ',$url));
    return($new);
  }
}
class documentClass{
 public static function cacheFile($time,$file,$src){
    if(file_exists($file)){
      if($time>=date ("Y-m-d H:i:s", filemtime($file))){
        $chw = curl_init();  
        curl_setopt($chw,CURLOPT_URL,$src);
        curl_setopt($chw,CURLOPT_RETURNTRANSFER,true);
        $json_outputw=curl_exec($chw);
        unlink($file);
        $myfile = fopen($file, "w");
        fwrite($myfile, $json_outputw);
        fclose($myfile);
      }
    } else {
      $chw = curl_init();  
      curl_setopt($chw,CURLOPT_URL,$src);
      curl_setopt($chw,CURLOPT_RETURNTRANSFER,true);
      $json_outputw=curl_exec($chw);
      $myfile = fopen($file, "w");
      fwrite($myfile, $json_outputw);
      fclose($myfile);
    }
    return $file;
  }
}
class inputClass{
  public static function isEmail($email){
    return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
  }
  public static function isUserID($username){
    if (preg_match('/^[a-z\d_]{5,20}$/i', $username)) {
      return true;
    } else {
      return false;
    }
  }  
  public static function isURL($url){
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
      return true;
    } else {
      return false;
    }
  }
  public static function checkPwd($x,$y)
  {
    if(empty($x) || empty($y) ) { return false; }
    if (strlen($x) < 4 || strlen($y) < 4) { return false; }
    if (strcmp($x,$y) != 0) {
      return false;
    }
    return true;
  }
  public static function PwdHash($pwd)
  {
    $options = array('cost' => 11);
    return password_hash($pwd, PASSWORD_BCRYPT, $options);
  }  
  public static function filter($data) {
    $data = trim(htmlentities(strip_tags($data)));
    if (get_magic_quotes_gpc())
      $data = stripslashes($data);  
    return $data;
  }
  public static function encode($num) {
    $scrambled = (240049382*$num + 37043083) % 308915753;
    return base_convert($scrambled, 10, 26);
  }
  public static function ChopStr($str, $len){
    if (strlen($str) < $len)
      return $str;
    $str = substr($str,0,$len);
    if ($spc_pos = strrpos($str," "))
      $str = substr($str,0,$spc_pos);
    return $str . "...";
  }
}
class sessionClass{
  public static function page_protect() {
    session_start();
    if (isset($_SESSION['HTTP_USER_AGENT']))
    {
      if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT']))
      {
        sessionClass::logout();
        exit;
      }
    }
    if (!isset($_SESSION['user_id']) && !isset($_SESSION['user']) )
    {
      if(isset($_COOKIE['user_id']) && isset($_COOKIE['user'])){
        $cookie_user_id  = inputClass::filter($_COOKIE['user_id']);
        $json = file_get_contents('../data/user.json');
        $zobj = json_decode($json,true);
		
        if($cookie_user_id==$zobj['id']){
           session_regenerate_id();   
            $_SESSION['user_id'] = $_COOKIE['user_id'];
            $_SESSION['user'] = $_COOKIE['user'];
            $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
        }
      } else {
        header("Location: login.php"); 
        exit();
      }
    }
  }
  public static function logout()
  {
    session_start();
    if(isset($_SESSION['user_id']) || isset($_COOKIE['user_id'])) {
      
	  
    }      
    unset($_SESSION['user_id']);
    unset($_SESSION['user']);
    unset($_SESSION['HTTP_USER_AGENT']);
    session_unset();
    session_destroy();
    $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie("user_id", '', time()-60*60*24*COOKIE_TIME_OUT, "/", $domain);
    setcookie("user", '', time()-60*60*24*COOKIE_TIME_OUT, "/", $domain);
    setcookie("user_key", '', time()-60*60*24*COOKIE_TIME_OUT, "/", $domain);
    header("Location: login.php"); 
  }
  
}
?>
