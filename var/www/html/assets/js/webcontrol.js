!function($) {
  "use strict";
  var Sidemenu = function() {
    this.$body = $("body"),
      this.$openLeftBtn = $(".open-left"),
        this.$menuItem = $("#sidebar-menu a")
          };
  Sidemenu.prototype.openLeftBar = function() {
    $("#wrapper").toggleClass("enlarged");
   // $("#wrapper").addClass("forced");
  },
    Sidemenu.prototype.init = function() {
      var $this  = this;
      var ua = navigator.userAgent,
          event = (ua.match(/iP/i)) ? "touchstart" : "click";
      this.$openLeftBtn.on(event, function(e) {
        e.stopPropagation();
        $this.openLeftBar();
      });
      $this.$menuItem.on(event, $this.menuItemClick);
      $("#sidebar-menu ul li.has_sub a.active").parents("li:last").children("a:first").addClass("active").trigger("click");
    },
      $.Sidemenu = new Sidemenu, $.Sidemenu.Constructor = Sidemenu    
        }(window.jQuery),
          function($) {
            "use strict";
            changeptype();
            $('.animate-number').each(function(){
              $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-duration"))); 
            });
            $('.right-bar-toggle').on('click', function(e){
              $('#wrapper').toggleClass('right-bar-enabled');
            }); 
            $.Sidemenu.init();
          }(window.jQuery),
            function executeFunctionByName(functionName, context /*, args */) {
              var args = [].slice.call(arguments).splice(2);
              var namespaces = functionName.split(".");
              var func = namespaces.pop();
              for(var i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
              }
              return context[func].apply(this, args);
            }
var w,h,dw,dh;
function changeptype(){
  w = $(window).width();
  h = $(window).height();
  dw = $(document).width(); 
  dh = $(document).height();
  if(w > 990){
    $("body").removeClass("smallscreen").addClass("widescreen");
    $("#wrapper").removeClass("enlarged");
  }else{
    $("body").removeClass("widescreen").addClass("smallscreen");
    $("#wrapper").addClass("enlarged");
    $(".left ul").removeAttr("style");
  }
  if($("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left")){
    $("body").removeClass("fixed-left").addClass("fixed-left-void");
  }else if(!$("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left-void")){
    $("body").removeClass("fixed-left-void").addClass("fixed-left");
  }
}
var debounce = function(func, wait, immediate) {
  var timeout, result;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) result = func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) result = func.apply(context, args);
    return result;
  };
}
    var wow = new WOW(
      {
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 50,
        mobile: false
      }
    );
wow.init();
function showthis(thishide,thisshow){ 
  var arrh = thishide.split(';'); 
  var arrsh = thisshow.split(';'); 
  $.each( arrh, function( key, val ) { if($('.'+val)[0]) {  $('.'+val).hide(); } });
  $.each( arrsh, function( key, value ) { if($('.'+value)[0]) {  $('.'+value).show(); } });
};
function rentext(thisid,thisname){
  $("#"+thisid).html(thisname);
};
function notify(message, type){ $.notify({ message: message },{ type: type, allow_dismiss: false, showProgressbar: true,label: 'Cancel', className: 'btn-xs btn-primary', placement: { from: 'top',align: 'right'},delay: 3000,animate: {enter: 'animated fadeIn', exit: 'animated fadeOut'}, offset: { x: 20,y: 85 }});};
$(function(){
  if($('.chkbb')[0]) { 
    var all_checkbox_divs = $('.chkbb');
    for (var i=0;i<all_checkbox_divs.length;i++) {  
      all_checkbox_divs[i].onclick = function (e) {
        var div_id = this.id;  var checkbox_id =div_id.split("_")[0];  var checkbox_element = $("#"+checkbox_id)[0];    
        if (checkbox_element.checked == true) {  
          checkbox_element.checked = false;  $("#"+div_id).attr('class', 'chkbb btn waves-effect'); }
        else {  checkbox_element.checked = true;    $("#"+div_id).attr('class', 'chkbb btn btn-success waves-effect'); }};
    }
  }
  if($('.polink')[0]) {
    $('.polink').popover({placement:'top',trigger:'hover'});
  }
  if($('.tolink')[0]) {
    $('.tolink').tooltip({placement:'top'});;
  }
});
$(document).ready(function() {
  var anchor = window.location.hash;
  $(anchor).collapse('toggle');
});
