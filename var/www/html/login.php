<?php 
  session_start();
  $err = array();
  $msg = array();
  $page="login";
  include "conf/config.php";
  foreach($_GET as $key => $value) {
    $get[$key] = inputClass::filter($value);
  }
  if(isset($_POST['doLogin']))
  {
    foreach($_POST as $key => $value) {
      $data[$key] = inputClass::filter($value);
    }
    $json = file_get_contents('data/user.json');
    $zobj = json_decode($json,true);
    $email = htmlspecialchars($data['user']);
    $pass = $data['pwd']; 
     if (password_verify($pass, $zobj['pass'])) { 
        session_start();
        session_regenerate_id (true);
        $_SESSION['user_id']= $zobj['id'];
        $_SESSION['user'] = $zobj['user'];
		$_SESSION['usraddress'] = $zobj['address'];
        $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
        $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
        setcookie("user_id", $_SESSION['user_id'], time()+1*60*24*COOKIE_TIME_OUT, "/", $domain);
	setcookie("usraddress", $_SESSION['usraddress'], time()+1*60*24*COOKIE_TIME_OUT, "/", $domain);
        setcookie("user",$_SESSION['user'], time()+1*60*24*COOKIE_TIME_OUT, "/", $domain);
        header("Location: index.php"); 
        } else { 
          $err[] = "Wrong password";  
        }
      
  }   ?>
<?php include "modules/headcontent.php";?>
</head>
<body style="background:transparent;">
  <div class="wrapper-page">
    <div class="text-center">
      <a href="/?" class="logo logo-lg"><img src="assets/images/logo.svg" style="width:40px;">&nbsp; <span>WebControl</span> </a>
    </div><br>
    <?php  if(!empty($err))  {  echo "<div class='alert alert-danger'>";  foreach ($err as $e) {    echo "$e";  }  echo "</div>"; }  ?>
    <?php if(!empty($msg))  {  echo "<div class='alert alert-info'>"; foreach ($msg as $e) { echo "$e<br>";  } echo "</div>";  } ?>
    <form class="form-horizontal" action="" method="post">
      <div class="form-group">
        <div class="row">
          <div class="col-md-12">
            <input class="form-control" name="usr_email" type="text" required autofocus placeholder="Email">
            <i class="fa fa-envelope-o form-control-feedback l-h-34"></i>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-12">
            <input class="form-control" name="pwd" type="password" required="" placeholder="Password">
            <i class="fa fa-lock form-control-feedback l-h-34"></i>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-6">
            <button class="btn btn-primary btn-block btn-custom w-md waves-effect waves-light" name="doLogin" type="submit"><i class="fa fa-unlock"></i>&nbsp;Log In
            </button>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12" style="text-align:center;padding:5px 10px;">
          <a href="https://webcontrol.link/register" class="text-muted"><i class="fa fa-user-plus m-r-5"></i>&nbsp;Create an account</a>
        </div>
      </div>
    </form>
  </div>
  <?php include "modules/js.php";?>
</body>
</html><?php include "modules/template_end.php";?>
