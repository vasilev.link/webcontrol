<?php  
  session_start();
  $err = array();
  $msg = array();
  include "conf/config.php";
  $page="dashboard";
  sessionClass::page_protect();
  include "modules/headcontent.php";?>
</head>
<body class="fixed-left">
  <div id="wrapper">
    <?php include "modules/menu.php";?>
    <div class="content-page">
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="page-title-box">
                <ol class="breadcrumb pull-right">
                  <li class="active">Dashboard</li>
                </ol>
                <h4 class="page-title">&nbsp;</h4>
              </div>
            </div>
          </div>
        <?php if(isset($_GET['sid'])){ 
  
  $jsoncontrollers = file_get_contents('data/controllers.json');
  $zobjcontrollers = json_decode($jsoncontrollers,true);
  $jsonrooms = file_get_contents('data/rooms.json');
  $zobjrooms = json_decode($jsonrooms,true);

if($zobjrooms[$_GET['sid']]['devices']){ ?>
<div class="row">
  <div class="col-md-6">
     <div class="card-box">
<?php $i=0; foreach($zobjrooms[$_GET['sid']]['devices'] as $key=>$val){ 
	$valdatasn=$zobjcontrollers["outputs"][$val['type']][$val['key']]; 
	$st='<div class="row" style="padding-bottom:5px;">';
	$st433='<div class="row">';
	
if($val['type']=="piface"){ ?>
                  <?php echo $st;?><div class="col-md-10">
                    <?php if($valdatasn['val']=="1"){ ?>  
                    <button type="button" class="waves-effect btn btn-success btn-block butpf<?php echo $valdatasn['output']?> tl" name="butoff" onclick="local('off','<?php echo $valdatasn['output']?>','piface','pf<?php echo $valdatasn['output']?>','<?php echo $zobjcontrollers['uid'];?>','2');"><i class="fa fa-power-off"></i>&nbsp;Turn <?php echo $valdatasn['info'];?> Off</button>
                    <?php } else { ?>
                    <button type="button" class="waves-effect btn btn-danger btn-block butpf<?php echo $valdatasn['output']?> tl" name="buton" onclick="local('on','<?php echo $valdatasn['output']?>','piface','pf<?php echo $valdatasn['output']?>','<?php echo $zobjcontrollers['uid'];?>','2');"><i class="fa <?php if(!empty($valdatasn['icon'])){echo $valdatasn['icon'];} else { echo "fa-play";} ?>"></i>&nbsp;Turn <?php echo $valdatasn['info'];?> On</button>
                    <?php } ?>
                  </div>
                  <div class="col-md-2 stout" style="text-align:center;font-size:1.7em;vertical-align:middle;">
                    <i class="fa fa-<?php echo (($valdatasn['val']=="1") ? "check g" : "close r");?>" id="waiticonpf<?php echo $valdatasn['output']?>"></i>
                  </div>  
                </div>
                <?php
} elseif($val['type']=="433"){ $i++;  ?>
              <?php if($i % 2 == 1){echo $st433;} ;?> <div class="col-md-6">
                    <button type="button" class="waves-effect btn btn-<?php echo (!empty($valdatasn['buttype'])? $valdatasn['buttype']: "info");?> btn-block but433<?php echo $val['key'];?> tl tl433" name="but433" onclick="local('on','<?php echo $valdatasn['code']?>','433','433<?php echo $val['key'];?>','<?php echo $zobjcontrollers['uid'];?>','<?php echo $valdatasn['val'];?>');"><i class="fa <?php echo (!empty($valdatasn['icon'])? $valdatasn['icon']: "fa-wifi");?>"></i>&nbsp;<?php echo $valdatasn['info'];?></button>
                  </div>
             <?php if($i % 2 == 0){echo '</div>';} ;?>    
<?php }
	     ?>


   <?php  } ?>
</div></div></div>
<?php } else { ?>
<div class="row"> <div class="col-md-6"><div class="card-box"><div class="alert alert-danger">No info for devices added yet</div></div></div></div>
<?php } ?>

		 
<?php } else { 
  $json = file_get_contents('data/rooms.json');
    $zobj = json_decode($json,true);

	
  if(!empty($zobj)){ ?>
  <div class="row"><?php 
    foreach($zobj as $key=>$val) { ?>     
        <div class="col-md-6">
          <div class="panel widget">
            <a href="<?php echo "/index.php?/&sid=".$key;?>" class="waves-effect" style="display:block;box-shadow:none;">
            <div class="row row-table row-flush">
              <div class="col-xs-4 bg-info text-center">
                <em class="fa fa-sign-in fa-3x"></em>
              </div>
              <div class="col-xs-8">
                <div class="panel-body text-left">
                  <h2><?php echo $val['name'];?></h2>
                </div>
              </div>
            </div>
            </a>
          </div>
        </div>
        <?php } ?></div><?php  } else { echo '<div class="alert alert-danger">You have no rooms added yet. Please go add them in the Configuration panel.</div>';}
 } ?>
        </div>
      </div>
<?php include "modules/footer.php";?>
    </div>
  </div>
<?php include "modules/js.php";?>
</body>
</html>
<?php include "modules/template_end.php";?>
