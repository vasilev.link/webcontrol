<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap-growl.min.js" type="text/javascript"></script>
<script src="assets/js/sweet-alert.min.js" type="text/javascript"></script>
<script src="assets/js/webcontrol.js"></script>
<?php if($page!="login"){?>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.waypoints.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>
<?php }; ?>
<?php if(!empty($_SESSION['user'])){?>
<script type="text/javascript">
<?php if(!empty($msg)){?> $(document).ready(function(){ notify('<?php echo $msg;?>', 'success'); });<?php } ?>
<?php if(!empty($err)){?> $(document).ready(function(){ notify('<?php echo $err;?>', 'danger'); });<?php } ?>
</script><?php } ?>
<script type="text/javascript">
 function local(contrl,output,type,thiskey,thunid,thisb){ 
    var dataString = 't='+ type+'&o='+output+'&cntrl='+contrl+'&uid='+thunid+'&board='+thisb+'&okey='+thiskey;
    $.ajax({
      type:"POST",
      url:"api.php?action=control",
      data: dataString,
      dataType: 'json',
      success:function(result){ 
        notify(result.log, 'success');
        if(contrl=="433"){
          $(".but"+thiskey).addClass( "btn-info" );
          setTimeout(function () {
            $(".but"+thiskey).removeClass("btn-info"); }, 4000);
        } else {
          $(".but"+thiskey).removeClass("btn-danger").addClass( "btn-info" );
          $(".but"+thiskey).html("<i class=\"fa fa-refresh spin\"></i>&nbsp;Please wait");
          $("#waiticon"+thiskey).removeClass().addClass( "fa fa-exclamation-triangle bl" );
          setTimeout(function () {
            window.location.href = '<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>'; }, 4000);
        }
      }
    });
  }
  </script>
