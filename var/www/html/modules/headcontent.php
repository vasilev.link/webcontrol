<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="description" content="WebControl - Control your house over the web :: Internet of Things builder">
    <meta name="author" content="Vasilev.link">
    <link rel="shortcut icon" href="favicon.ico">
    <title>WebControl - Control your house over the web :: Internet of Things project builder</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/wccp.php" rel="stylesheet" type="text/css">
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="assets/css/sweet-alert.css" rel="stylesheet" type="text/css">
    <script src="assets/js/modernizr.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
