<div class="topbar">
  <div class="topbar-left">
    <div class="text-center">
      <a href="index.php" class="waves-effect logo"><img src="assets/images/logo.png" style="width:40px;">&nbsp;<span>WebControl</span> </a>
    </div>
  </div>
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="">
        <div class="pull-left">
          <button class="button-menu-mobile open-left waves-effect">
            <i class="fa fa-bars"></i>
          </button>
          <span class="clearfix"></span>
        </div>
        <ul class="nav navbar-nav navbar-right pull-right">
          <li>
            <a href="logout.php" class="right-bar-toggle waves-effect waves-light"><i class="fa fa-lock"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="left side-menu">
  <div class="sidebar-inner">
    <div id="sidebar-menu">
      <ul>
        <li><a href="index.php" class="waves-effect<?php if($page=="dashboard"){ echo " active";} ?>"><i class="fa fa-dashboard"></i><span> Dashboard </span></a></li>
        <li><a href="controllers.php" class="waves-effect<?php if($page=="controllers"){ echo " active";} ?>"><i class="fa fa-server"></i><span> Controllers</span></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
