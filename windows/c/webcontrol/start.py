from Crypto.Cipher import AES
from base64 import b64encode, b64decode
import sys, json

content="message.."
passphrase="key.."
iv="suid.."

key=passphrase.encode('utf-8')
iv = iv.encode('utf-8')

cipher=AES.new(key=key,mode=AES.MODE_CBC,IV=iv)
body = cipher.decrypt(b64decode(str(content))).decode('utf-8')
body=json.loads(body.replace('\0',''))
print(body['num'])
